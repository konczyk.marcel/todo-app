# MTODO app
Application for managing your everyday tasks.

## Getting started
You have 2 options to setup and run this project. 

### Using [docker & docker-compose](https://docs.docker.com/compose/install/)

To build or re-build container run following command:
```sh
docker compose build
```
---
To start entire app run following command:
```sh
docker compose up
```
then open [http://localhost:3000](http://localhost:3000) to view it in the browser.

---
To stop entire app and remove containers run following command:
```sh
docker compose down
```

### Running as standalone instances.
Check [backend/README.md](backend/README.md) and [web/README.md](web/README.md) for further instructions.

## Project structure
```
├── README.md          - this file
├── backend            - api layer of application
│   ├── README.md      - instruction for running standalone api
│   ├── ...
├── docker-compose.yml - docker compose main configuration
├── mongo_seed         - seed for creating initial data for application
│   ├── seed_db.sh     - script for (re)creating MongoDB seed
│   ├── ... 
├── web                - web layer of application
│   ├── README.md      - instruction for running standalone web
│   ├── cypress        - e2e/component tests of web layer
│   ├── ...            
```