/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './src/**/*.{js,jsx,ts,tsx}',
  ],
  theme: {
    extend: {
      colors: {
        primary: '#242F9B',
        secondary: '#646FD4',
        secondaryShade: '#9BA3EB',
        secondaryTins: '#DBDFFD',
        white: '#FFF',
        lightGray: '#D9D9D9'
      },
    },
  },
  plugins: [],
}
