/// <reference types="cypress" />
describe('Welcome Page', () => {
	beforeEach(() => {
		cy.visit(Cypress.env('baseUrl'))
	})
	
	it('redirect to start page', () => {
		cy.get('[data-cy="get-started"]')
			.should('be.visible')
			.and('contain', 'Get Started')
		cy.get('[data-cy="get-started"]').click()
		cy.location().should((location) => {
			expect(location.href).to.eq(Cypress.env('baseUrl') + '/start')
		})
	})
})

export {}