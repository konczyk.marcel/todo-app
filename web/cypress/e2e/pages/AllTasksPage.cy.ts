/// <reference types="cypress" />

import {
	aliasMutation,
	aliasQuery,
	hasOperationName,
} from '../../utils/graphql-test-utils'

describe('All Tasks Page', () => {
	beforeEach(() => {
		cy.intercept('POST', Cypress.env('graphqlUrl'), (req) => {
			if (hasOperationName(req, 'GetLists')) {
				aliasQuery(req, 'GetLists')
				req.reply({ fixture: 'GetListsMock.json' })
			} else if (hasOperationName(req, 'CreateList')) {
				aliasMutation(req, 'CreateList')
				req.reply({ fixture: 'CreateListMock.json' })
			}
		})
		cy.clock(new Date(2022, 10, 14, 9, 0, 0))
		cy.visit(Cypress.env('baseUrl') + '/all-tasks')
	})
	
	it('displays correct page data', () => {
		cy.get('[data-cy="total-tasks"]')
			.should('be.visible')
			.and('contain.text', 'You have 6 tasks overall!')
		cy.get('[data-cy="date"]')
			.should('be.visible')
			.and('contain.text', 'Mon Nov 14 2022')
		cy.get('[data-cy="filter-today"]')
			.should('be.visible')
			.and('contain.text', 'Today')
		cy.get('[data-cy="filter-week"]')
			.should('be.visible')
			.and('contain.text', 'Week')
		cy.get('[data-cy="filter-month"]')
			.should('be.visible')
			.and('contain.text', 'Month')
		cy.get('[data-cy="lists"]')
			.children()
			.should('have.length', 4)
	})
	
	it('filter should work', () => {
		checkLists([['Home', 2], ['Work', 4], ['Goals', 0]])
		
		cy.get('[data-cy="filter-today"]').click()
		cy.get('[data-cy="total-tasks"]')
			.should('contain.text', 'You have 0 tasks today!')
		checkLists([['Home', 0], ['Work', 0], ['Goals', 0]])
		
		cy.get('[data-cy="filter-week"]').click()
		cy.get('[data-cy="total-tasks"]')
			.should('contain.text', 'You have 1 tasks this week!')
		checkLists([['Home', 1], ['Work', 0], ['Goals', 0]])
		
		cy.get('[data-cy="filter-month"]').click()
		cy.get('[data-cy="total-tasks"]')
			.should('contain.text', 'You have 5 tasks this month!')
		checkLists([['Home', 2], ['Work', 3], ['Goals', 0]])
		
		cy.get('[data-cy="filter-month"]').click()
		cy.get('[data-cy="total-tasks"]')
			.should('contain.text', 'You have 6 tasks overall!')
		checkLists([['Home', 2], ['Work', 4], ['Goals', 0]])
	})
	
	it('should redirect to list view', () => {
		cy.get('[data-cy="list-Home"').click()
		cy.location().should((location) => {
			expect(location.href).to.eq(Cypress.env('baseUrl') + '/list' + '/636b77ced3b2a34583e3186e')
		})
	})
	
	it('should show correct data in create new list modal', () => {
		cy.get('[data-cy="create-list"]').click()
		cy.get('[data-cy="create-form"]')
			.should('be.visible')
		cy.get('[data-cy="create-icons"]')
			.children()
			.should('have.length', 10)
		cy.get('[data-cy="create-colors"]')
			.children()
			.should('have.length', 10)
		cy.get('[data-cy="submit-list"]')
			.should('be.visible')
			.and('have.text', 'Create')
		cy.get('[data-cy="submit-list"]').click()
		cy.get('[data-cy="form-title-feedback"]')
			.should('be.visible')
			.and('have.text', 'Required. Needs to be at least 3 characters long.')
		cy.get('[data-cy="form-icon-feedback"]')
			.should('be.visible')
			.and('have.text', 'Required. Please select a list icon.')
		cy.get('[data-cy="form-color-feedback"]')
			.should('be.visible')
			.and('have.text', 'Required. Please select a background color.')
	})
	
	it('should create new list', () => {
		cy.get('[data-cy="create-list"]').click()
		cy.get('[data-cy="form-title"]').type('Test')
		cy.get('[data-cy="form-icon"]').first().click()
		cy.get('[data-cy="form-color"]').first().click()
		cy.get('[data-cy="submit-list"]').click()
	})
	
	const checkLists = (lists: Array<[string, number]>) => {
		lists.forEach(([title, amount]) => {
			cy.get(`[data-cy="list-${ title }"] [data-cy="list-title"]`)
				.should('have.text', title)
			cy.get(`[data-cy="list-${ title }"] [data-cy="list-tasks-amount"]`)
				.should('have.text', amount)
		})
	}
})
