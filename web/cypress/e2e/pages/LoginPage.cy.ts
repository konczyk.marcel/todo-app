/// <reference types="cypress" />
describe('Login Page', () => {
	beforeEach(() => {
		cy.visit(Cypress.env('baseUrl') + '/login')
	})
	
	it('displays all login page actions', () => {
		cy.get('[data-cy="email"]').should('be.visible')
		cy.get('[data-cy="password"]').should('be.visible')
		cy.get('[data-cy="submit"]').should('be.visible')
		cy.get('[data-cy="sign-up-link"]').should('be.visible')
	})
	
	it('displays correct feedback messages if provided input is incorrect', () => {
		cy.get('[data-cy="email"]').type('testgmail.com')
		cy.get('[data-cy="password"]').type('12')
		cy.get('[data-cy="submit"]').click()
		cy.get('[data-cy="email-feedback"]')
			.should('be.visible')
			.and('contain', 'Required. Incorrect email format.')
		cy.get('[data-cy="password-feedback"]')
			.should('be.visible')
			.and('contain', 'Required. Needs to be at least 3 characters long.')
	})
	
	it('log user in if provided data is valid', () => {
		cy.get('[data-cy="email"]').type('test@gmail.com')
		cy.get('[data-cy="password"]').type('test123')
		cy.get('[data-cy="submit"]').click()
		cy.location().should((location) => {
			expect(location.href).to.eq(Cypress.env('baseUrl') + '/all-tasks')
		})
	})
	
	it('redirect to sign up page', () => {
		cy.get('[data-cy="sign-up-link"]').click()
		cy.location().should((location) => {
			expect(location.href).to.eq(Cypress.env('baseUrl') + '/sign-up')
		})
	})
})

export {}
