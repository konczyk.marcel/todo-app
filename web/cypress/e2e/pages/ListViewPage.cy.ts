/// <reference types="cypress" />

import {
	aliasMutation,
	aliasQuery,
	hasOperationName,
} from '../../utils/graphql-test-utils'

describe('List View Page', () => {
	beforeEach(() => {
		cy.intercept('POST', Cypress.env('graphqlUrl'), (req) => {
			if (hasOperationName(req, 'GetList')) {
				aliasQuery(req, 'GetList')
				req.reply({ fixture: 'GetListMock.json' })
			} else if (hasOperationName(req, 'CreateTask')) {
				aliasMutation(req, 'CreateTask')
				req.reply({ fixture: 'CreateTaskMock.json' })
			} else if (hasOperationName(req, 'RemoveList')) {
				aliasMutation(req, 'RemoveList')
				req.reply({ fixture: 'RemoveListMock.json' })
			} else if (hasOperationName(req, 'UpdateTask')) {
				aliasMutation(req, 'UpdateTask')
				req.reply({ fixture: 'UpdateTaskMock.json' })
			} else if (hasOperationName(req, 'RemoveTask')) {
				aliasMutation(req, 'RemoveTask')
				req.reply({ fixture: 'RemoveTaskMock.json' })
			}
		})
		cy.clock(new Date(2022, 10, 14, 15, 0, 0))
		cy.visit(Cypress.env('baseUrl') + '/list' + '/636b77ced3b2a34583e3186e')
	})
	
	it('displays page data', () => {
		cy.get('[data-cy="page-title"]')
			.should('have.text', 'Home')
		cy.get('[data-cy="tasks"]')
			.children()
			.should('have.length', 2)
	})
	
	it('should show/hide finished tasks', () => {
		cy.get('[data-cy="tasks"]')
			.children()
			.should('have.length', 2)
		cy.get('[data-cy="toggle-finished"]')
			.should('have.text', 'Show done (1)')
		cy.get('[data-cy="toggle-finished"]').click()
		cy.get('[data-cy="tasks"]')
			.children()
			.should('have.length', 3)
		cy.get('[data-cy="toggle-finished"]')
			.should('have.text', 'Hide done (1)')
	})
	
	it('should show correct data in create new task modal', () => {
		cy.get('[data-cy="create-task"]').click()
		cy.get('[data-cy="save-form"]')
			.should('be.visible')
		cy.get('[data-cy="submit-task"]')
			.should('be.visible')
			.and('have.text', 'Save')
		cy.get('[data-cy="modal-title"]')
			.should('be.visible')
			.and('have.text', 'Create task')
		cy.get('[data-cy="submit-task"]').click()
		cy.get('[data-cy="form-title-feedback"]')
			.should('be.visible')
			.and('have.text', 'Required. Needs to be at least 3 characters long.')
		cy.get('[data-cy="form-description-feedback"]')
			.should('be.visible')
			.and('have.text', 'Required.')
		cy.get('[data-cy="form-deadline-feedback"]')
			.should('be.visible')
			.and('have.text', 'Deadline cannot be in the past.')
	})
	
	it('should save new task', () => {
		cy.get('[data-cy="create-task"]').click()
		cy.get('[data-cy="form-title"]').type('Test task')
		cy.get('[data-cy="form-description"]').type('Test task description.')
		cy.get('[data-cy="form-deadline"]').type('2022-11-14T18:30')
		cy.get('[data-cy="submit-task"]').click()
	})
	
	it('should remove list', () => {
		cy.get('[data-cy="remove-list"]')
			.should('be.visible')
		cy.get('[data-cy="remove-list"]').click()
		cy.location().should((location) => {
			expect(location.origin).to.eq(Cypress.env('baseUrl'))
			expect(location.pathname).to.eq('/all-tasks')
			expect(location.search).to.eq('?refetch=true')
		})
	})
	
	it('should show correct data in edit task modal', () => {
		cy.get('[data-cy="task-edit-toggle"]')
			.first()
			.click()
		cy.get('[data-cy="task-edit"]').click()
		cy.get('[data-cy="save-form"]')
			.should('be.visible')
		cy.get('[data-cy="form-title"]')
			.should('have.value', 'Windows')
		cy.get('[data-cy="form-description"]')
			.should('have.value', 'Clean all windows')
		cy.get('[data-cy="form-deadline"]')
			.should('have.value', '2022-11-14T10:15')
		cy.get('[data-cy="submit-task"]')
			.should('be.visible')
			.and('have.text', 'Save')
		cy.get('[data-cy="modal-title"]')
			.should('be.visible')
			.and('have.text', 'Edit task')
		cy.get('[data-cy="close-modal"]').click()
	})
	
	it('should edit task', () => {
		cy.get('[data-cy="task-edit-toggle"]')
			.first()
			.click()
		cy.get('[data-cy="task-edit"]').click()
		cy.get('[data-cy="form-deadline"]').type('2022-11-15T10:15')
		cy.get('[data-cy="submit-task"]').click()
	})
	
	it('should remove task', () => {
		cy.get('[data-cy="task-edit-toggle"]')
			.first()
			.click()
		cy.get('[data-cy="task-remove"]').click()
	})
})
