/// <reference types="cypress" />
describe('Start Page', () => {
	beforeEach(() => {
		cy.visit(Cypress.env('baseUrl') + '/start')
	})
	
	it('redirect to sign up page', () => {
		cy.get('[data-cy="sign-up-link"]')
			.should('be.visible')
			.and('contain', 'SIGN UP')
		cy.get('[data-cy="sign-up-link"]').click()
		cy.location().should((location) => {
			expect(location.href).to.eq(Cypress.env('baseUrl') + '/sign-up')
		})
	})
	
	it('redirect to login page', () => {
		cy.get('[data-cy="login-link"]')
			.should('be.visible')
			.and('contain', 'LOG IN')
		cy.get('[data-cy="login-link"]').click()
		cy.location().should((location) => {
			expect(location.href).to.eq(Cypress.env('baseUrl') + '/login')
		})
	})
})

export {}
