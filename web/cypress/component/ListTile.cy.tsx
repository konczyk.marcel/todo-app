import ListTile from 'components/ListTile'

describe('ListTile component', () => {
	beforeEach(function () {
		cy.fixture('ListTileMock.json').then((list) => {
			this.list = list
		})
	})
	
	it('display correct data', function () {
		cy.mount(<ListTile list={ this.list as List } filter={ null } />)
		cy.get(`[data-cy=list-${ this.list.title }] [data-cy="list-container"]`)
			.should('have.class', this.list.bgColor)
		cy.get(`[data-cy=list-${ this.list.title }] [data-cy="list-tasks-amount"]`)
			.should('have.text', '3')
		cy.get(`[data-cy=list-${ this.list.title }] [data-cy="list-title"]`)
			.should('have.text', 'Home')
		cy.get(`[data-cy=list-${ this.list.title }] [data-cy="list-icon"]`)
			.should('be.visible')
	})
})