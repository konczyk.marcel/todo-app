import TaskRow      from 'components/TaskRow'
import { DateTime } from 'luxon'

describe('TaskRow component', () => {
	beforeEach(function () {
		cy.fixture('TaskRowMock.json').then((task) => {
			this.task = task
		})
	})
	
	it('display correct data', function () {
		const onEditSpy = cy.spy().as('onEditSpy')
		const onRemoveSpy = cy.spy().as('onRemoveSpy')
		const onStatusChangeSpy = cy.spy().as('onStatusChangeSpy')
		const onSetTaskIdToEditSpy = cy.spy().as('onSetTaskIdToEditSpy')
		cy.mount(<TaskRow
			onEdit={ onEditSpy }
			onRemove={ onRemoveSpy }
			onStatusChange={ onStatusChangeSpy }
			setTaskIdToEdit={ onSetTaskIdToEditSpy }
			task={ this.task }
			taskIdToEdit={ this.task._id }
		/>)
		cy.get(`[data-cy=task-title]`)
			.should('be.visible')
			.and('have.text', this.task.title)
		cy.get(`[data-cy=task-description]`)
			.should('be.visible')
			.and('have.text', this.task.description)
		cy.get(`[data-cy=task-deadline] div:first-child`)
			.should('be.visible')
			.and('have.text', DateTime.fromMillis(+this.task.deadline).toFormat('HH:mm'))
		cy.get(`[data-cy=task-deadline] div:last-child`)
			.should('be.visible')
			.and('have.text', DateTime.fromMillis(+this.task.deadline).toFormat('dd LLL yy'))
		cy.get('[data-cy="task-finished"]').click()
		cy.get('@onStatusChangeSpy')
			.should('have.been.called', 1)
		cy.get('[data-cy="task-edit-toggle"]').click()
		cy.get('@onSetTaskIdToEditSpy')
			.should('have.been.called', 1)
		cy.get('[data-cy="task-edit"]')
			.should('be.visible')
		cy.get('[data-cy="task-edit"]').click()
		cy.get('@onEditSpy')
			.should('have.been.called', 1)
		cy.get('[data-cy="task-remove"]').click()
		cy.get('@onRemoveSpy')
			.should('have.been.called', 1)
	})
})