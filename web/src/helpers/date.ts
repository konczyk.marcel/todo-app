import { DateTime } from 'luxon'

export const dateFilter = (filter: FILTER_TYPE, deadline: string) =>
	filter
	? DateTime.fromMillis(+deadline).hasSame(DateTime.now(), filter)
	: true

export const isDateOverdue = (millis: string | number) => DateTime.fromMillis(+millis)
																																	.diffNow('minutes').minutes < 0
export const isDatePast = (isoDate: string) => DateTime.fromISO(isoDate)
																											 .diffNow('minutes').minutes > 0
