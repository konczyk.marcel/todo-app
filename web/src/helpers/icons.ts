import { IconDefinition } from '@fortawesome/free-regular-svg-icons'
import {
	faBook,
	faBuilding,
	faGraduationCap,
	faHouse,
	faPhone,
	faRunning,
	faShoppingCart,
	faSuitcase,
	faSuitcaseMedical,
	faUtensils,
}                         from '@fortawesome/free-solid-svg-icons'

export const allIconsMapper: { [index: string]: IconDefinition } = {
	'book': faBook,
	'graduation-cap': faGraduationCap,
	'suitcase-medical': faSuitcaseMedical,
	'person-running': faRunning,
	'shopping-cart': faShoppingCart,
	'suitcase': faSuitcase,
	'building': faBuilding,
	'utensils': faUtensils,
	'house': faHouse,
	'phone': faPhone,
}

export const allListColors = [
	'bg-cyan-600',
	'bg-sky-400',
	'bg-amber-800',
	'bg-blue-600',
	'bg-lime-600',
	'bg-amber-600',
	'bg-indigo-300',
	'bg-emerald-500',
	'bg-pink-600',
	'bg-yellow-500',
]