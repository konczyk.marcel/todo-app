import {
	Link,
	useNavigate,
	useParams,
}                          from 'react-router-dom'
import { DateTime }        from 'luxon'
import React, {
	useMemo,
	useState,
}                          from 'react'
import ArrowBackSVG        from 'assets/icons/ArrowBackSVG'
import CirclePlusSVG       from 'assets/icons/CirclePlusSVG'
import CirclesSVG          from 'assets/images/CirclesSVG'
import MtodoModal          from 'components/MtodoModal'
import {
	useMutation,
	useQuery,
}                          from '@apollo/client'
import { useForm }         from 'react-hook-form'
import { zodResolver }     from '@hookform/resolvers/zod'
import * as z              from 'zod'
import {
	CREATE_TASK,
	GET_LIST,
	REMOVE_LIST,
	REMOVE_TASK,
	UPDATE_TASK,
	UPDATE_TASK_STATUS,
}                          from 'mtodo-gql'
import { isDatePast }      from 'helpers/date'
import TaskRow             from 'components/TaskRow'
import { useToggle }       from 'usehooks-ts'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTrash }         from '@fortawesome/free-solid-svg-icons'

const ListViewPage = (): JSX.Element => {
	const { listId } = useParams()
	const [showFinished, toggleShowFinish] = useToggle()
	const [modalIsOpen, , setIsOpen] = useToggle()
	const [taskIdToEdit, setTaskIdToEdit] = useState<string>('')
	const navigate = useNavigate()
	
	const { data, refetch } = useQuery(GET_LIST, { variables: { listId } })
	const [createTask] = useMutation(CREATE_TASK)
	const [updateTask] = useMutation(UPDATE_TASK)
	const [updateTaskStatus] = useMutation(UPDATE_TASK_STATUS)
	const [deleteTask] = useMutation(REMOVE_TASK)
	const [deleteList] = useMutation(REMOVE_LIST)
	
	const {
		register,
		handleSubmit,
		formState: { errors },
		reset,
		setValue,
	} = useForm({ resolver: zodResolver(schema) })
	
	const removeList = async (evt: any) => {
		evt.preventDefault()
		await deleteList({
			variables: {
				_id: listId,
			},
		})
		navigate('/all-tasks?refetch=true')
	}
	
	const saveTask = async (form: Partial<Task>) => {
		await (taskIdToEdit ? editTask(taskIdToEdit, form) : addTask(form))
		await refetch()
		reset()
		onModalClose()
	}
	
	const addTask = async (form: Partial<Task>) => {
		await createTask({
			variables: {
				listId: listId,
				title: form.title,
				description: form.description,
				deadline: new Date(form.deadline || '').toISOString(),
			},
		})
	}
	
	const editTask = async (taskId: string, form: Partial<Task>) => {
		await updateTask({
			variables: {
				_id: taskId,
				title: form.title,
				description: form.description,
				deadline: new Date(form.deadline || '').toISOString(),
			},
		})
	}
	
	const changeTaskStatus = async (task: Task) => {
		await updateTaskStatus({
			variables: {
				_id: task._id,
				finished: !task.finished,
			},
		})
		await refetch()
	}
	
	const removeTask = async (task: Task) => {
		await deleteTask({
			variables: {
				_id: task._id,
			},
		})
		setTaskIdToEdit('')
		await refetch()
	}
	
	const openEditModal = async (task: Task) => {
		setTaskIdToEdit(task._id)
		setValue('title', task.title)
		setValue('description', task.description)
		setValue('deadline', DateTime.fromMillis(+(task.deadline)).toFormat('yyyy-MM-dd\'T\'HH:mm:ss'))
		setIsOpen(true)
	}
	
	const onModalClose = () => {
		setIsOpen(false)
		reset()
		setTaskIdToEdit('')
	}
	
	const doneTasksAmount = data?.list?.tasks.filter((task: Task) => task.finished).length || '0'
	
	const tasksView = useMemo(() => {
		const filtered = data?.list?.tasks.filter((task: Task) => showFinished || !task.finished)
		return filtered?.length > 0
					 ? filtered
						 .sort((a: Task, b: Task) => +a.deadline - +b.deadline)
						 .sort((a: Task, b: Task) => +a.finished - +b.finished)
						 .map((task: Task) => <TaskRow
							 task={ task }
							 onEdit={ openEditModal }
							 taskIdToEdit={ taskIdToEdit }
							 setTaskIdToEdit={ setTaskIdToEdit }
							 onStatusChange={ changeTaskStatus }
							 onRemove={ removeTask }
							 key={ task._id }
						 />)
					 : <div className="text-center text-white">There are no tasks.</div>
	}, [data, taskIdToEdit, showFinished])
	
	return <>
		<div className="w-full animate__animated animate__fadeIn">
			<div className="absolute top-0 z-[-1] scale-125">
				<CirclesSVG />
			</div>
			<div className="grid w-full h-[75px] px-5 grid-cols-3 grid-flow-col-dense w-full place-items-center">
				<Link to={ '/all-tasks' } className="hover:opacity-80 cursor-pointer col-span-1 justify-self-start">
					<ArrowBackSVG />
				</Link>
				<div className="text-white font-bold text-2xl place-self-center" data-cy="page-title">
					{ data?.list?.title || 'Unknown' }
				</div>
				<div
					className="col-span-1 text-red-800 scale-[150%] justify-self-end mr-3 mt-1 cursor-pointer hover:opacity-70"
					onClick={ removeList }
					data-cy="remove-list"
				>
					{/*<PencilSVG />*/ }
					<FontAwesomeIcon icon={ faTrash } size="sm" className="text-white" />
				</div>
			</div>
			
			<div className="grid auto-rows-max gap-5 h-[calc(100vh-75px-100px)] pb-10 overflow-y-auto scrollbar-hide w-full px-5" data-cy="tasks">
				{ tasksView }
			</div>
			
			<div className="grid bg-gray-50 h-[100px] w-full border-t-2 border-lightGray grid-cols-3 place-items-center">
				<div />
				<div
					className="cursor-pointer hover:opacity-90"
					onClick={ () => setIsOpen(true) }
					data-cy="create-task"
				>
					<CirclePlusSVG />
				</div>
				<div
					className="font-semibold cursor-pointer hover:opacity-70"
					onClick={ toggleShowFinish }
					data-cy="toggle-finished"
				>
					{ showFinished ? 'Hide' : 'Show' } done ({ doneTasksAmount })
				</div>
			</div>
			
			<MtodoModal modalIsOpen={ modalIsOpen } setIsOpen={ setIsOpen }>
				<form className="grid gap-5" onSubmit={ handleSubmit(saveTask) } data-cy="save-form">
					<div className="font-semibold text-center text-xl" data-cy="modal-title">{ taskIdToEdit ? 'Edit' : 'Create' } task</div>
					<div>
						<input
							className="appearance-none block w-full bg-gray-200 text-gray-700 border border-lightGray rounded-2xl py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
							placeholder="Title"
							{ ...register('title') }
							data-cy="form-title"
						/>
						<div className="text-xs text-red-600 ml-2" data-cy="form-title-feedback">
							{ errors.title?.message?.toString() }
						</div>
					</div>
					<div>
						<input
							className="appearance-none block w-full bg-gray-200 text-gray-700 border border-lightGray rounded-2xl py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
							placeholder="Description"
							{ ...register('description') }
							data-cy="form-description"
						/>
						<div className="text-xs text-red-600 ml-2" data-cy="form-description-feedback">
							{ errors.description?.message?.toString() }
						</div>
					</div>
					<div>
						<input
							className="appearance-none block w-full bg-gray-200 text-gray-700 border border-lightGray rounded-2xl py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
							type="datetime-local"
							{ ...register('deadline') }
							data-cy="form-deadline"
						/>
						<div className="text-xs text-red-600 ml-2" data-cy="form-deadline-feedback">
							{ errors.deadline?.message?.toString() }
						</div>
					</div>
					<div className="flex gap-5 justify-center">
						<button
							className="bg-gray-400 hover:opacity-95 mt-5 rounded-2xl py-1 px-5 text-white text-xl text-center"
							onClick={ onModalClose }
							type="reset"
							data-cy="close-modal"
						>Cancel
						</button>
						<button
							className="bg-secondary hover:opacity-95 mt-5 rounded-2xl py-1 px-5 text-white text-xl text-center"
							type="submit"
							data-cy="submit-task"
						>
							Save
						</button>
					</div>
				</form>
			</MtodoModal>
		</div>
	</>
}

const schema = z.object({
	title: z.string()
					.min(3, { message: 'Required. Needs to be at least 3 characters long.' }),
	description: z.string()
								.min(1, { message: 'Required.' }),
	deadline: z.string()
						 .refine(isDatePast, { message: 'Deadline cannot be in the past.' }),
})

export default ListViewPage