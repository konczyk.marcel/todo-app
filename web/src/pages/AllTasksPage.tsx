import React, { useState } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPlus }          from '@fortawesome/free-solid-svg-icons'
import { useLocation }     from 'react-router-dom'
import MtodoModal          from 'components/MtodoModal'
import ListTile            from 'components/ListTile'
import Filter              from 'components/Filter'
import Search              from 'components/Search'
import {
	useMutation,
	useQuery,
}                          from '@apollo/client'
import { useForm }         from 'react-hook-form'
import { zodResolver }     from '@hookform/resolvers/zod'
import * as z              from 'zod'
import {
	CREATE_LIST,
	GET_LISTS,
}                          from 'mtodo-gql'
import {
	allIconsMapper,
	allListColors,
}                          from 'helpers/icons'
import { dateFilter }      from 'helpers/date'
import CirclesSVG          from 'assets/images/CirclesSVG'
import {
	useEffectOnce,
	useToggle,
}                          from 'usehooks-ts'

const AllTasksPage = (): JSX.Element => {
	const [filter, setFilter] = useState<FILTER_TYPE>(null)
	const [modalIsOpen, , setIsOpen] = useToggle()
	
	const { data, refetch } = useQuery(GET_LISTS)
	const [createList] = useMutation(CREATE_LIST)
	
	const location = useLocation()
	
	const {
		register,
		handleSubmit,
		formState: { errors },
		reset,
		setValue,
		getValues,
		trigger,
	} = useForm({ resolver: zodResolver(schema) })
	
	useEffectOnce(() => {
		if (location.search.includes('refetch=true')) refetch()
	})
	
	const addList = async (form: Partial<List>) => {
		await createList({
			variables: {
				title: form.title,
				bgColor: form.bgColor,
				iconName: form.iconName,
			},
		})
		await refetch()
		onModalClose()
	}
	
	const onModalClose = () => {
		setIsOpen(false)
		reset()
	}
	
	const totalTasks = data?.lists?.reduce(
		(acc: number, list: List) => acc + list
			?.tasks
			?.filter((task: Task) => !task.finished && dateFilter(filter, task.deadline)).length || 0
		, 0) || 0
	
	return <>
		<div className="grid place-items-center animate__animated animate__fadeIn">
			<div className="absolute top-0 z-[-1] scale-125">
				<CirclesSVG />
			</div>
			<div className="font-semibold mt-5 text-2xl text-white">
				Mtodo Logo
			</div>
			<div className="font-semibold text-primary text-xl mt-5" data-cy="total-tasks">
				You have <span className="text-white text-3xl">{ totalTasks } tasks</span> { getTimeInterval(filter) }!
			</div>
			<div className="text-md mt-2 text-primary font-semibold" data-cy="date">
				{ new Date().toDateString() }
			</div>
			<div className="mt-5 w-fit">
				<Search />
			</div>
			
			<div className="mt-10">
				<Filter
					value={ filter }
					setFilter={ setFilter }
				/>
			</div>
			
			<div className="mt-10 grid gap-5 grid-cols-3" data-cy="lists">
				{
					data?.lists?.map((list: List) => <ListTile
						list={ list }
						filter={ filter }
						key={ list._id }
					/>)
				}
				<div
					className="w-[100px] h-[100px] border-2 border-purple-400 px-5 pb-3 pt-2 grid place-items-center rounded-2xl cursor-pointer hover:opacity-80"
					onClick={ () => setIsOpen(true) }
					data-cy="create-list"
				>
					<div className="align-bottom">
						<FontAwesomeIcon icon={ faPlus } size="2xl" className="text-purple-400" />
					</div>
				</div>
			</div>
			
			<MtodoModal modalIsOpen={ modalIsOpen } setIsOpen={ setIsOpen }>
				<form className="grid gap-5" onSubmit={ handleSubmit(addList) } data-cy="create-form">
					<div className="font-semibold text-center text-xl">Add new list</div>
					<div>
						<input
							className="appearance-none block w-full bg-gray-200 text-gray-700 border border-lightGray rounded-2xl py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
							placeholder="List name"
							{ ...register('title') }
							data-cy="form-title"
						/>
						<div className="text-xs text-red-600 ml-2" data-cy="form-title-feedback">
							{ errors.title?.message?.toString() }
						</div>
					</div>
					<hr />
					<div>
						<div className="grid gap-5 grid-flow-col-dense grid-rows-2 justify-items-center" data-cy="create-icons">
							{
								Object.values(allIconsMapper).map(
									(icon) => <FontAwesomeIcon
										icon={ icon }
										key={ icon.iconName }
										size="xl"
										color="black"
										data-cy="form-icon"
										className={ (getValues('iconName') === icon.iconName
																 ? ' border-black'
																 : '') + ' w-[25px] h-[25px] p-2 rounded-xl border-2 cursor-pointer hover:opacity-70' }
										onClick={ () => {
											setValue('iconName', icon.iconName)
											trigger('iconName')
										} }
									/>)
							}
						</div>
						<div className="text-xs text-red-600 ml-2 mt-3" data-cy="form-icon-feedback">
							{ errors.iconName?.message?.toString() }
						</div>
					</div>
					<hr />
					<div>
						<div className="grid gap-5 grid-flow-col-dense grid-rows-2 justify-items-center" data-cy="create-colors">
							{
								allListColors.map((color) => <div
									className={ color + (getValues('bgColor') === color
																			 ? ' border-black'
																			 : '') + ' w-[40px] h-[40px] border-2 border-lightGray grid place-items-center rounded-xl cursor-pointer hover:opacity-80' }
									key={ color }
									onClick={ () => {
										setValue('bgColor', color)
										trigger('bgColor')
									} }
									data-cy="form-color"
								/>)
							}
						</div>
						<div className="text-xs text-red-600 ml-2 mt-3" data-cy="form-color-feedback">
							{ errors.bgColor?.message?.toString() }
						</div>
					</div>
					<div className="flex gap-5 justify-center">
						<button
							className="bg-gray-400 hover:opacity-95 mt-5 rounded-2xl py-1 px-5 text-white text-xl text-center"
							onClick={ onModalClose }
							type="reset"
						>Cancel
						</button>
						<button className="bg-secondary hover:opacity-95 mt-5 rounded-2xl py-1 px-5 text-white text-xl text-center" type="submit"
							data-cy="submit-list">Create</button>
					</div>
				</form>
			</MtodoModal>
		</div>
	</>
}

const getTimeInterval = (filter: FILTER_TYPE) => {
	switch (filter) {
		case 'day':
			return 'today'
		case 'week':
			return 'this week'
		case 'month':
			return 'this month'
	}
	return 'overall'
}

const schema = z.object({
	title: z.string()
					.min(3, { message: 'Required. Needs to be at least 3 characters long.' })
					.max(7, { message: 'Required. Needs to be max 7 characters long.' }),
	iconName: z.string({ required_error: 'Required. Please select a list icon.' })
						 .refine((name) => Object.keys(allIconsMapper).includes(name)),
	bgColor: z.string({ required_error: 'Required. Please select a background color.' })
						.refine((color) => color.startsWith('bg-')),
})

export default AllTasksPage