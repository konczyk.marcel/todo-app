import {
	Link,
	useNavigate,
}                      from 'react-router-dom'
import GoogleBtn       from 'assets/buttons/GoogleBtn'
import FacebookBtn     from 'assets/buttons/FacebookBtn'
import * as z          from 'zod'
import { useForm }     from 'react-hook-form'
import { zodResolver } from '@hookform/resolvers/zod'
import React           from 'react'

const LoginPage = (): JSX.Element => {
	const {
		register,
		handleSubmit,
		formState: { errors },
	} = useForm<LoginFormData>({ resolver: zodResolver(schema) })
	
	const navigate = useNavigate()
	
	const loginUser = (form: LoginFormData) => {
		console.log('form: ', form)
		navigate('/all-tasks')
	}
	
	return (
		<div className="grid place-items-center">
			<div className="font-semibold text-2xl mt-10 text-secondaryShade justify-self-start">
				Mtodo Logo
			</div>
			<div className="font-semibold text-2xl mt-10 text-secondary">Hello Again!</div>
			<div className="font-semibold text-xl text-secondaryShade">Welcome Back</div>
			<form action="" className="mt-10 w-full" onSubmit={ handleSubmit(loginUser) }>
				<div>
					<input
						className="appearance-none block w-full bg-gray-200 text-gray-700 border border-lightGray rounded-2xl py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
						placeholder="Enter email"
						{ ...register('email') }
						data-cy="email"
					/>
					<div className="text-xs text-red-600 ml-2 mb-4" data-cy="email-feedback">
						{ errors.email?.message?.toString() }
					</div>
				</div>
				<div>
					<input
						className="appearance-none block w-full bg-gray-200 text-gray-700 border border-lightGray rounded-2xl py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
						type="password"
						placeholder="Password"
						{ ...register('password') }
						data-cy="password"
					/>
					<div className="text-xs text-red-600 ml-2 mb-1" data-cy="password-feedback">
						{ errors.password?.message?.toString() }
					</div>
				</div>
				
				<button
					type="submit"
					className="bg-secondary hover:opacity-95 w-full mt-5 rounded-2xl py-2 text-white text-xl text-center"
					data-cy="submit"
				>
					Login
				</button>
			</form>
			
			<div className="font-light text-sm py-5">
				Or Continue With
			</div>
			
			<div className="flex gap-5">
				<div className="cursor-pointer hover:opacity-90"><GoogleBtn /></div>
				<div className="cursor-pointer hover:opacity-90"><FacebookBtn /></div>
			</div>
			
			<div className="mt-3">
				<span className="font-thin">Not a member?</span>
				<Link
					to="/sign-up"
					className="ml-2 text-secondary hover:opacity-80"
					data-cy="sign-up-link"
				>Register now</Link>
			</div>
		</div>
	)
}

interface LoginFormData {
	email: string
	password: string
}

const schema = z.object({
	email: z.string().email({ message: 'Required. Incorrect email format.' }),
	password: z.string().min(3, { message: 'Required. Needs to be at least 3 characters long.' }),
})

export default LoginPage