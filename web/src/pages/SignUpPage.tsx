import {
	Link,
	useNavigate,
}                      from 'react-router-dom'
import * as z          from 'zod'
import { useForm }     from 'react-hook-form'
import { zodResolver } from '@hookform/resolvers/zod/dist/zod'
import React           from 'react'

const SignUpPage = (): JSX.Element => {
	const {
		register,
		handleSubmit,
		formState: { errors },
	} = useForm<SignUpFormData>({ resolver: zodResolver(schema) })
	const navigate = useNavigate()
	
	const registerUser = (form: SignUpFormData) => {
		console.log('form: ', form)
		navigate('/all-tasks')
	}
	
	return (
		<div className="grid place-items-center">
			<div className="font-semibold text-2xl mt-10 text-secondaryShade justify-self-start">
				Mtodo Logo
			</div>
			<div className="font-semibold text-2xl mt-10 text-secondaryShade">Hello!</div>
			<div className="text-center font-light mt-3 text-secondaryShade">
				welcome to Mtodo app <br />
				sign up to get started.
			</div>
			<form action="" className="mt-5 w-full" onSubmit={ handleSubmit(registerUser) }>
				<div>
					<input
						className="appearance-none block w-full bg-gray-200 text-gray-700 border border-lightGray rounded-2xl py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
						type="text"
						placeholder="Your name"
						data-cy="name"
						{ ...register('name') }
					/>
					<div className="text-xs text-red-600 ml-2 mb-4" data-cy="name-feedback">
						{ errors.name?.message?.toString() }
					</div>
				</div>
				<div>
					<input
						className="appearance-none block w-full bg-gray-200 text-gray-700 border border-lightGray rounded-2xl py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
						placeholder="Enter email"
						data-cy="email"
						{ ...register('email') }
					/>
					<div className="text-xs text-red-600 ml-2 mb-4" data-cy="email-feedback">
						{ errors.email?.message?.toString() }
					</div>
				</div>
				<div>
					<input
						className="appearance-none block w-full bg-gray-200 text-gray-700 border border-lightGray rounded-2xl py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
						type="password"
						placeholder="Password"
						data-cy="password"
						{ ...register('password') }
					/>
					<div className="text-xs text-red-600 ml-2 mb-4" data-cy="password-feedback">
						{ errors.password?.message?.toString() }
					</div>
				</div>
				
				<div className="font-light text-sm ml-2">
					by signing up you agree to our <Link to="#" className="text-secondary hover:opacity-70">terms & CONDITIONS </Link>
					of use and <Link to="#" className="text-secondary hover:opacity-70">privacy policy</Link>.
				</div>
				<button
					type="submit"
					className="bg-secondary w-full hover:opacity-95 mt-5 rounded-2xl py-2 text-white text-xl text-center"
					data-cy="submit"
				>
					SIGN UP
				</button>
			</form>
			<div className="mt-3">
				<span className="font-thin">Already have an account?</span>
				<Link
					to="/login"
					className="ml-2 underline text-secondary hover:opacity-80"
					data-cy="login-link"
				>LOG IN</Link>
			</div>
		</div>
	)
}

interface SignUpFormData {
	name: string
	email: string
	password: string
}

const schema = z.object({
	name: z.string().min(2, { message: 'Required. Needs to be at least 2 characters long.' }),
	email: z.string().email({ message: 'Required. Incorrect email format.' }),
	password: z.string().min(3, { message: 'Required. Needs to be at least 3 characters long.' }),
})

export default SignUpPage