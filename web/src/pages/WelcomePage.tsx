import { Link }   from 'react-router-dom'
import WelcomeSVG from 'assets/images/WelcomeSVG'

const WelcomePage = (): JSX.Element => {
	return (
		<div className="grid place-items-center">
			<div className="max-w-sm">
				<WelcomeSVG />
			</div>
			<div className="font-semibold text-2xl mt-10">Manage your tasks</div>
			<div className="text-center font-light px-2 mt-3">
				Organize, plan, and collaborate on tasks with Mtodo. Your busy life deserves this. You can manage
				checklist and your goal.
			</div>
			<Link to="/start"
						className="bg-secondary hover:opacity-95 w-4/5 mt-5 rounded-2xl py-2 text-white text-xl text-center"
						data-cy="get-started">
				Get Started
			</Link>
		</div>
	)
}

export default WelcomePage