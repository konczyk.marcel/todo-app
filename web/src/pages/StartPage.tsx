import { Link } from 'react-router-dom'
import StartSVG from 'assets/images/StartSVG'

const StartPage = (): JSX.Element => {
	return (
		<div className="grid place-items-center">
			<div className="max-w-sm">
				<StartSVG />
			</div>
			<div className="font-semibold text-2xl mt-10">Start Manage Your Task With</div>
			<div className="font-semibold text-3xl mt-2 text-secondaryShade">Mtodo</div>
			<Link to="/sign-up"
						className="bg-secondary hover:opacity-95 w-4/5 mt-5 rounded-2xl py-2 text-white text-xl text-center"
						data-cy="sign-up-link"
			>
				SIGN UP
			</Link>
			<div className="mt-3">
				<span className="font-light">Already have an account?</span>
				<Link to="/login"
							className="ml-2 underline text-secondary hover:opacity-80"
							data-cy="login-link"
				>LOG IN</Link>
			</div>
		</div>
	)
}

export default StartPage