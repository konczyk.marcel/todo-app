import { gql } from '@apollo/client'

export const GET_LISTS = gql`
    query GetLists {
        lists {
            _id
            bgColor
            iconName
            title
            tasks {
                _id
                deadline
                finished
            }
        }
    }
`

export const GET_LIST = gql`
    query GetList($listId: ID!) {
        list(_id: $listId) {
            _id
            title
            tasks {
                _id
                title
                description
                deadline
                finished
            }
        }
    }
`

export const CREATE_LIST = gql`
    mutation CreateList($title: String!, $bgColor: String!, $iconName: String!) {
        createList(title: $title, bgColor: $bgColor, iconName: $iconName) {
            _id
            title
            bgColor
            iconName
        }
    }
`

export const UPDATE_LIST = gql`
    mutation UpdateList($_id: ID!, $title: String!, $bgColor: String!, $iconName: String!) {
        updateList(_id: $_id, title: $title, bgColor: $bgColor, iconName: $iconName) {
            _id
            title
            bgColor
            iconName
        }
    }
`

export const REMOVE_LIST = gql`
    mutation RemoveList($_id: ID!) {
        deleteList(_id: $_id)
    }
`

export const CREATE_TASK = gql`
    mutation CreateTask($listId: ID!, $title: String!, $description: String!, $deadline: String!) {
        createTask(listId: $listId, title: $title, description: $description, deadline: $deadline) {
            _id
            title
            description
            deadline
            finished
        }
    }
`

export const UPDATE_TASK = gql`
    mutation UpdateTask($_id: ID!, $title: String!, $description: String!, $deadline: String!) {
        updateTask(_id: $_id, title: $title, description: $description, deadline: $deadline) {
            _id
            title
            description
            deadline
            finished
        }
    }
`

export const UPDATE_TASK_STATUS = gql`
    mutation UpdateTask($_id: ID!, $finished: Boolean!) {
        updateTaskStatus(_id: $_id, finished: $finished) {
            _id
            finished
        }
    }
`

export const REMOVE_TASK = gql`
    mutation RemoveTask($_id: ID!) {
        deleteTask(_id: $_id)
    }
`