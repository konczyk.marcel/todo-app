import React             from 'react'
import { DateTime }      from 'luxon'
import { isDateOverdue } from 'helpers/date'
import CircleCheckSVG    from 'assets/icons/CircleCheckSVG'
import VerticalDotsSVG   from 'assets/icons/VerticalDotsSVG'

interface TaskRowProps {
	task: Task,
	onEdit: CallableFunction,
	taskIdToEdit: string,
	setTaskIdToEdit: CallableFunction,
	onRemove: CallableFunction,
	onStatusChange: CallableFunction,
}

const TaskRow = ({
	task,
	onEdit,
	onRemove,
	onStatusChange,
	taskIdToEdit,
	setTaskIdToEdit,
}: TaskRowProps) => {
	return (<>
		<div
			className="bg-white drop-shadow-xl shadow-xl flex place-items-center gap-3 rounded-xl p-3 h-[100px]"
			key={ task._id }
			data-cy={ 'task-' + task.title }
		>
			<div
				className={ (isDateOverdue(task.deadline) ? 'text-red-600 ' : '') + 'text-xs font-light text-center' }
				data-cy="task-deadline"
			>
				<div className="whitespace-nowrap">
					{ DateTime.fromMillis(+task.deadline).toFormat('HH:mm') }
				</div>
				<div className="whitespace-nowrap">
					{ DateTime.fromMillis(+task.deadline).toFormat('dd LLL yy') }
				</div>
			</div>
			<div className="flex place-items-center gap-3">
				<div
					className="hover:opacity-80 cursor-pointer"
					onClick={ () => onStatusChange(task) }
					data-cy="task-finished"
				>
					<CircleCheckSVG checked={ task.finished } />
				</div>
				<div className="">
					<div className="text-md" data-cy="task-title">{ task.title }</div>
					<div className="text-sm font-light" data-cy="task-description">{ task.description }</div>
				</div>
			</div>
			<div className="relative ml-auto">
				<div
					className="hover:opacity-80 cursor-pointer"
					onClick={ () => setTaskIdToEdit((pv: string) => pv === task._id ? '' : task._id) }
					data-cy="task-edit-toggle"
				>
					<VerticalDotsSVG />
				</div>
				{
					task._id === taskIdToEdit
					? <div
						className="absolute left-[-30px] bg-white rounded-xl drop-shadow-xl border-2 text-sm font-light animate__animated animate__fadeIn animate__faster"
					>
						<div
							className="cursor-pointer rounded-xl px-3 hover:bg-gray-100"
							onClick={ () => onEdit(task) }
							data-cy="task-edit"
						>Edit
						</div>
						<div
							className="cursor-pointer rounded-xl px-3 hover:bg-gray-100"
							onClick={ () => onRemove(task) }
							data-cy="task-remove"
						>Remove
						</div>
					</div>
					: <></>
				}
			</div>
		</div>
	</>)
}

export default TaskRow