import Modal                 from 'react-modal'
import React, { ReactNode } from 'react'

interface MtodoModalProps {
	modalIsOpen: boolean,
	setIsOpen: CallableFunction,
	children: ReactNode
}

const MtodoModal = ({ modalIsOpen, setIsOpen, children }: MtodoModalProps): JSX.Element => {
	return <>
		<Modal
			isOpen={ modalIsOpen }
			onRequestClose={ () => setIsOpen(false) }
			style={ customModalStyles }
			ariaHideApp={ false }
		>
			{ children }
		</Modal>
	</>
}

const customModalStyles = {
	overlay: {
		backdropFilter: 'blur(3px)',
		boxShadow: '10px 3px 1px #9E9E9E',
	},
	content: {
		border: '1px solid lightgray',
		borderRadius: '15px',
		top: '50%',
		left: '50%',
		right: 'auto',
		bottom: 'auto',
		marginRight: '-50%',
		transform: 'translate(-50%, -50%)',
		minWidth: '375px',
	},
}

export default MtodoModal