import React               from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faQuestion }      from '@fortawesome/free-solid-svg-icons'
import { Link }            from 'react-router-dom'
import { dateFilter }      from 'helpers/date'
import { allIconsMapper }  from 'helpers/icons'

interface ListTileProps {
	list: List,
	filter: FILTER_TYPE
}

const ListTile = ({ list, filter }: ListTileProps): JSX.Element => {
	const tasksAmount = list?.tasks
													?.filter((e: Task) => !e.finished && dateFilter(filter, e.deadline)).length || 0
	
	return (<>
		<Link to={ '/list/' + list._id } data-cy={ 'list-' + list.title }>
			<div
				className={ list.bgColor + ' w-[100px] h-[100px] px-5 pb-3 pt-2 grid gap-1 justify-items-center rounded-2xl cursor-pointer hover:opacity-90 animate__animated animate__fadeIn' }
				data-cy="list-container"
			>
				<div
					className="text-white text-right justify-self-end h-[15px]"
					data-cy="list-tasks-amount"
				>
					{ tasksAmount }
				</div>
				<FontAwesomeIcon
					icon={ allIconsMapper[list.iconName] || faQuestion }
					size="xl"
					color="white"
					data-cy="list-icon"
				/>
				<div
					className="text-white font-bold text-sm"
					data-cy="list-title"
				>
					{ list.title }</div>
			</div>
		</Link>
	</>)
}

export default ListTile