import React from 'react'

const Search = (): JSX.Element => {
	// TODO
	
	return <>
		<input
			type="text"
			className="px-7 py-2.5 mx-auto bg-white rounded-xl shadow-xl drop-shadow-lg"
			placeholder="Search tasks"
		/>
	</>
}

export default Search