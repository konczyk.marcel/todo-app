import React from 'react'

interface FilterProps {
	value: FILTER_TYPE
	setFilter: CallableFunction
}

const Filter = ({ value, setFilter }: FilterProps): JSX.Element => {
	return (<>
		<div className="flex gap-5">
			<button
				className={
					'border-2 px-3 py-1 rounded-2xl border-secondary hover:bg-secondary hover:cursor-pointer active:bg-secondary '
					+ (value === 'day' ? 'bg-secondaryShade' : '')
				}
				onClick={ () => setFilter((pv: FILTER_TYPE) => pv === 'day' ? null : 'day') }
				data-cy="filter-today"
			>Today
			</button>
			<button
				className={
					'border-2 px-3 py-1 rounded-2xl border-secondary hover:bg-secondary hover:cursor-pointer active:bg-secondary '
					+ (value === 'week' ? 'bg-secondaryShade' : '')
				}
				onClick={ () => setFilter((pv: FILTER_TYPE) => pv === 'week' ? null : 'week') }
				data-cy="filter-week"
			>Week
			</button>
			<button
				className={
					'border-2 px-3 py-1 rounded-2xl border-secondary hover:bg-secondary hover:cursor-pointer active:bg-secondary '
					+ (value === 'month' ? 'bg-secondaryShade' : '')
				}
				onClick={ () => setFilter((pv: FILTER_TYPE) => pv === 'month' ? null : 'month') }
				data-cy="filter-month"
			>Month
			</button>
		</div>
	</>)
}

export default React.memo(Filter)