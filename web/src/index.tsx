import React             from 'react'
import ReactDOM          from 'react-dom/client'
import App               from './App'
import reportWebVitals   from './reportWebVitals'
import {
	ApolloClient,
	InMemoryCache,
	ApolloProvider,
}                        from '@apollo/client'
import 'animate.css'
import './index.css'

const root = ReactDOM.createRoot(
	document.getElementById('root') as HTMLElement,
)

const apolloClient = new ApolloClient({
	uri: process.env.REACT_APP_GRAPHQL_URL || 'http://localhost:4000/graphql',
	cache: new InMemoryCache(),
	connectToDevTools: true,
})

root.render(
	<React.StrictMode>
		<ApolloProvider client={ apolloClient }>
			<App />
		</ApolloProvider>
	</React.StrictMode>,
)

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals()
