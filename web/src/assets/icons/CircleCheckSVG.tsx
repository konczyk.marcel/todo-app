

interface CircleCheckSVGProps {
	checked?: boolean
}

const CircleCheckSVG = ({ checked }:CircleCheckSVGProps) : JSX.Element => {
	return <>
		<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
			<circle cx="12" cy="12" r="11.5" stroke="#E7E7E7"/>
			{
				checked
				? <path d="M5 12.3333L9.66667 17L19 7" stroke="#8785F6" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"/>
				: <></>
			}
		</svg>
	</>
}

export default CircleCheckSVG