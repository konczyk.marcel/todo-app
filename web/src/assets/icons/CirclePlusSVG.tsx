const CirclePlusSVG = (): JSX.Element => {
	return <>
		<svg width="64" height="64" viewBox="0 0 64 64" fill="none" xmlns="http://www.w3.org/2000/svg">
			<g filter="url(#filter0_d_258_668)">
				<rect x="3" y="3" width="56" height="56" rx="28" fill="#646FD4" />
				<path d="M40.3334 32.3306H32.3334V40.3306H29.6667V32.3306H21.6667V29.6639H29.6667V21.6639H32.3334V29.6639H40.3334V32.3306Z" fill="white" />
			</g>
			<defs>
				<filter id="filter0_d_258_668" x="0" y="0" width="64" height="64" filterUnits="userSpaceOnUse" colorInterpolationFilters="sRGB">
					<feFlood floodOpacity="0" result="BackgroundImageFix" />
					<feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha" />
					<feOffset dx="1" dy="1" />
					<feGaussianBlur stdDeviation="2" />
					<feComposite in2="hardAlpha" operator="out" />
					<feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0" />
					<feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow_258_668" />
					<feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow_258_668" result="shape" />
				</filter>
			</defs>
		</svg>
	
	</>
}

export default CirclePlusSVG