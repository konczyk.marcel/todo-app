const CirclesSVG = () : JSX.Element => {
	return <>
		<svg width="360" height="252" viewBox="0 5 360 252" fill="none" xmlns="http://www.w3.org/2000/svg">
			<circle cx="250.5" cy="112.5" r="139.5" fill="url(#paint0_linear_603_703)"/>
			<circle cx="81.5" cy="90.5" r="117.5" fill="url(#paint1_radial_603_703)"/>
			<defs>
				<linearGradient id="paint0_linear_603_703" x1="244.5" y1="3" x2="349.5" y2="210.5" gradientUnits="userSpaceOnUse">
					<stop stopColor="#646FD4"/>
					<stop offset="0.553242" stopColor="#646FD4" stopOpacity="0.812201"/>
					<stop offset="1" stopColor="#646FD4" stopOpacity="0"/>
				</linearGradient>
				<radialGradient id="paint1_radial_603_703" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(132 51.5) rotate(111.631) scale(187.182)">
					<stop offset="0.00836083" stopColor="#646FD4"/>
					<stop offset="0.0208967" stopColor="#646FD4"/>
					<stop offset="0.620951" stopColor="#646FD4"/>
					<stop offset="0.943874" stopColor="#99A0E3"/>
					<stop offset="1" stopColor="white"/>
				</radialGradient>
			</defs>
		</svg>
	</>
}

export default CirclesSVG