interface Task {
	_id: string
	listId: string
	title: string
	description: string
	deadline: string
	finished: boolean
}

interface List {
	_id: string
	iconName: string
	title: string
	bgColor: string
	tasks: Array<Task>
}

type FILTER_TYPE = null | 'day' | 'week' | 'month'