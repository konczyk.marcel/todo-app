import React        from 'react'
import {
	createBrowserRouter,
	RouterProvider,
}                   from 'react-router-dom'
import WelcomePage  from 'pages/WelcomePage'
import SignUpPage   from 'pages/SignUpPage'
import LoginPage    from 'pages/LoginPage'
import StartPage    from 'pages/StartPage'
import AllTasksPage from 'pages/AllTasksPage'
import ListViewPage from 'pages/ListViewPage'

const router = createBrowserRouter([
	{
		path: '/',
		element: <WelcomePage />,
	},
	{
		path: '/start',
		element: <StartPage />,
	},
	{
		path: '/login',
		element: <LoginPage />,
	},
	{
		path: '/sign-up',
		element: <SignUpPage />,
	},
	{
		path: '/all-tasks',
		element: <AllTasksPage />,
	},
	{
		path: '/list/:listId',
		element: <ListViewPage />,
	},
])


const App = (): JSX.Element => {
	return (
		<div className="flex justify-center">
			<div className="max-w-sm w-full mx-5 mb-10 max-h-screen overscroll-y-contain max-h-screen">
				<RouterProvider router={ router } />
			</div>
		</div>
	)
}

export default App

