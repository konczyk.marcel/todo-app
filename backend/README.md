# MTODO app - backend
Backend client

## Requirements

- **Node 16 LTS** (`v16.13.1`)
- **npm** `v8.19.*`
- **MongoDB** instance available under uri: `mongodb://user:pass@localhost:27017/`

## Running mongo using docker
To run mongo as single instance in docker run following command in terminal:
```
docker run -d --name mtodo -p 27017:27017 -e MONGO_INITDB_ROOT_USERNAME=user -e MONGO_INITDB_ROOT_PASSWORD=pass mongo:latest
```

## Available Scripts

In the project directory, you can run:

### `npm run gen-types`
Generating graphql types required by application

### `npm start`
Runs the graphql server app in the development mode.
under http://localhost:4000/graphql

### `npm test`
Launches the [Cypress](https://docs.cypress.io/) tests.

