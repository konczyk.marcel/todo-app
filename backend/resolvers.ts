import {
	MutationCreateListArgs,
	MutationCreateTaskArgs,
	MutationDeleteListArgs,
	MutationDeleteTaskArgs,
	MutationUpdateListArgs,
	MutationUpdateTaskArgs,
	MutationUpdateTaskStatusArgs,
	QueryListArgs,
}           from './graphql-types'
import List from './models/List'
import Task from './models/Task'

export const resolvers = {
	Query: {
		list: async (_: any, { _id }: QueryListArgs) => {
			const list = await List.findById(_id).populate('tasks')
			if (list) return list
			throw new Error(`List with ID: ${ _id } not found`)
		},
		lists: async (_: any) => await List.find({}).populate('tasks'),
	},
	Mutation: {
		async createList(_: any, { title, bgColor, iconName }: MutationCreateListArgs) {
			return await List.create({ title, iconName, bgColor })
		},
		async updateList(_: any, { _id, title, bgColor, iconName }: MutationUpdateListArgs) {
			const list = await List.findById(_id)
			if (list) {
				await list.updateOne({ title, iconName, bgColor })
				return list
			}
			throw new Error(`List with ID: ${ _id } not found`)
		},
		async deleteList(_: any, { _id }: MutationDeleteListArgs) {
			const list = await List.findById(_id)
			if (list) {
				await list.remove()
				return `Item ${ _id } deleted with success`
			}
			throw new Error(`List with ID: ${ _id } not found`)
		},
		
		async createTask(_: any, { listId, title, description, deadline }: MutationCreateTaskArgs) {
			const list = await List.findById(listId)
			const task = await Task.create({ title, description, deadline, finished: false })
			if (list) {
				await list.updateOne({ tasks: [...list.tasks, task] })
				return task
			}
			throw new Error(`List with ID: ${ listId } not found`)
		},
		async updateTask(_: any, { _id, title, description, deadline }: MutationUpdateTaskArgs) {
			const task = await Task.findById(_id)
			if (task) {
				await task.updateOne({ title, description, deadline })
				return task
			}
			throw new Error(`Task with ID: ${ _id } not found`)
		},
		async updateTaskStatus(_: any, { _id, finished }: MutationUpdateTaskStatusArgs) {
			const task = await Task.findById(_id)
			if (task) {
				await task.updateOne({ finished })
				return task
			}
			throw new Error(`Task with ID: ${ _id } not found`)
		},
		async deleteTask(_: any, { _id }: MutationDeleteTaskArgs) {
			const task = await Task.findById(_id)
			if (task) {
				await task.remove()
				return `Task ${ _id } deleted with success`
			}
			throw new Error(`Task with ID: ${ _id } not found`)
		},
	},
}