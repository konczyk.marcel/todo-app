import * as fs          from 'fs'
import * as path        from 'path'
import { ApolloServer } from 'apollo-server-express'
import { resolvers }    from './resolvers'
import express          from 'express'
import * as mongoose    from 'mongoose'

const typeDefs = fs.readFileSync(path.join(__dirname, 'schema.graphql'), 'utf8').toString()

const mongoDB = process.env.DATABASE_URI || 'mongodb://user:pass@localhost:27017/'
const startServer = async () => {
	const app = express()
	const server = new ApolloServer({ typeDefs, resolvers })
	await server.start()
	
	try {
		await mongoose.connect(mongoDB)
	} catch (err) {
		console.log('err: ', err)
		console.error('Could not establish connection with mongodb instance.')
	}
	
	server.applyMiddleware({ app })
	app.listen({ port: 4000 }, () =>
		console.log(`🚀 Server ready at http://localhost:4000${ server.graphqlPath }`),
	)
}

startServer()