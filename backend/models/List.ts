import {
	Schema,
	model,
	Types,
}           from 'mongoose'
import Task from './Task'

const listSchema = new Schema(
	{
		title: {
			type: String,
			required: true,
		},
		iconName: {
			type: String,
			required: true,
		},
		bgColor: {
			type: String,
			required: true,
		},
		tasks: [
			{
				type: Types.ObjectId,
				ref: 'Task',
			},
		],
	},
)

listSchema.pre('remove', { document: true, query: true }, async function (next) {
	const tasks = this.get('tasks')
										.map((item: any) => item._id)
	await Task.deleteMany({ _id: { $in: tasks } })
	next()
})

const List = model('List', listSchema)

export default List