import {
	model,
	Schema,
}           from 'mongoose'
import List from './List'

const taskSchema = new Schema(
	{
		title: {
			type: String,
			required: true,
		},
		description: String,
		deadline: Date,
		finished: {
			type: Boolean,
			default: false,
		},
	},
)

taskSchema.pre('remove', { document: true, query: true }, async function (next) {
	await List.updateMany(
		{},
		{
			$pull: {
				tasks: this.get('_id'),
			},
		},
	)
	next()
})

const Task = model('Task', taskSchema)

export default Task