#!/bin/bash

sleep 10

mongoimport --drop --authenticationDatabase admin --uri "${DATABASE_URI}" --collection lists --file /lists.json --jsonArray
mongoimport --drop --authenticationDatabase admin --uri "${DATABASE_URI}" --collection tasks --file /tasks.json --jsonArray